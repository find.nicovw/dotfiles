-- TREESITTER
require'nvim-treesitter.configs'.setup {
	ensure_installed = {"c",  "vim", "go", "cpp", "lua", "bash", "python" },
	highlight = {
		enable = true,
	}
}

-- COMMENT
require('nvim_comment').setup {
	line_mapping = "<leader>c",
	operator_mapping = "<leader>oc",
	comment_chunk_text_object = "ic"
}

-- BLANKLINE INDENTATION
require("ibl").setup()

-- NVIM TREE
require("nvim-tree").setup{
    -- remove_keymaps={"<Tab>"},
}
vim.keymap.set('n', '<Space>f',':NvimTreeToggle<CR>' )

-- Telescope
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}

pcall(require('telescope').load_extension, 'fzf')
vim.keymap.set('n', '<leader>sb', require('telescope.builtin').buffers,      { desc = '[S]earch [B]buffers' })
vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files,   { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags,    { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string,  { desc = '[S]earch current [W]ord' })
vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep,    { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics,  { desc = '[S]earch [D]iagnostics' })


require("which-key").setup {}
