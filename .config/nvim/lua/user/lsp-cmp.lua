local lsp_zero = require('lsp-zero')

lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps({buffer = bufnr})
end)

require('mason').setup({})
require('mason-lspconfig').setup({
  -- ensure_installed = {'gopls', 'bashls'},
  handlers = {
    lsp_zero.default_setup,
  },
})

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()
  
cmp.setup({
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-f>'] = cmp_action.luasnip_jump_forward(),
    ['<C-b>'] = cmp_action.luasnip_jump_backward(),
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
  })
})


-- lsp.set_preferences({
--   suggest_lsp_servers = false,
--   sign_icons = {
--     error = 'E',
--     warn = 'W',
--     hint = 'H',
--     info = 'I'
--   }
-- })

-- lsp.on_attach(function(client, bufnr)
--   local opts = { buffer = bufnr, remap = false }
--   vim.keymap.set("n", "<Tab>1", function() vim.lsp.buf.definition() end, { desc = '[D]efinition' })
--   vim.keymap.set("n", "<Tab>2", function() vim.lsp.buf.hover() end, opts)
--   vim.keymap.set("n", "<Tab>3", function() vim.lsp.buf.workspace_symbol() end, opts)
--   vim.keymap.set("n", "<Tab>4", function() vim.diagnostic.open_float() end, opts)
--   vim.keymap.set("n", "<Tab>]", function() vim.diagnostic.goto_next() end, opts)
--   vim.keymap.set("n", "<Tab>[", function() vim.diagnostic.goto_prev() end, opts)
--   vim.keymap.set("n", "<Tab>7", function() vim.lsp.buf.code_action() end, opts)
--   vim.keymap.set("n", "<Tab>8", function() vim.lsp.buf.references() end, opts)
--   vim.keymap.set("n", "<Tab>r", function() vim.lsp.buf.rename() end, { desc = '[R]ename' })
--   vim.keymap.set("i", "<Tab>0", function() vim.lsp.buf.signature_help() end, opts)
-- end)
--
-- lsp.setup()
--
-- vim.diagnostic.config({
--   virtual_text = true
-- })
-- vim.diagnostic.config({
--   virtual_text = true
-- })
