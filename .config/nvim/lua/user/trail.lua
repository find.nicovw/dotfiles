-- Set 'listchars' for trailing spaces and tabs to use the same indicator
vim.opt.listchars = {
    trail = '█', -- Indicator for trailing spaces
    tab = '█ ',   -- Indicator for tabs
}

-- Enable listing of whitespace characters
vim.opt.list = true

-- Apply a highlight color for these indicators
vim.cmd [[highlight Whitespace guifg=Red ctermfg=Red]]

-- Function to enable displaying special characters
local function enable_special_chars()
    vim.opt.list = true
end

-- Function to disable displaying special characters
local function disable_special_chars()
    vim.opt.list = false
end

-- Autocommand to disable special characters display in insert mode
vim.api.nvim_create_autocmd("InsertEnter", {
    pattern = "*",
    callback = disable_special_chars,
})

-- Autocommand to enable special characters display when leaving insert mode
vim.api.nvim_create_autocmd("InsertLeave", {
    pattern = "*",
    callback = enable_special_chars,
})


