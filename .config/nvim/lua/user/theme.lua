-- THEME
vim.o.background = "dark"
vim.cmd("colorscheme monokai-pro")
-- LUALINE
require("lualine").setup{
	options = {
		icons_enabled = true,
		theme = "monokai-pro",
        component_separators = { left = '', right = ''},
        section_separators = { left = '', right = ''},
	},
}


