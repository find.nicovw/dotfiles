-----------------------------------------------------------------------------------------
vim.cmd.packadd("packer.nvim")
-----------------------------------------------------------------------------------------
return require("packer").startup(function(use)
	-- Packer can manage itself
	use "wbthomason/packer.nvim"

	-- look and feel
	use { "loctvl842/monokai-pro.nvim" }
	use { "lifepillar/vim-solarized8"}
	use { "nvim-lualine/lualine.nvim" }
	use { "nvim-tree/nvim-web-devicons" }

	-- quality of life
	use("nvim-treesitter/nvim-treesitter", { run = ":TSUpdate" })
	use { "nvim-telescope/telescope.nvim" }
	use { "nvim-lua/plenary.nvim" }
	use { "terrortylor/nvim-comment" }
	use { "tpope/vim-sleuth" }
	use { "lukas-reineke/indent-blankline.nvim" }
	use { "nvim-tree/nvim-tree.lua" }
	use { "folke/which-key.nvim" }

	-- Cmp
	use { "hrsh7th/nvim-cmp" }      -- The completion plugin
	use { "hrsh7th/cmp-buffer" }    -- buffer completions
	use { "hrsh7th/cmp-path" }      -- path completions
	use { "saadparwaiz1/cmp_luasnip" } -- snippet completions
	use { "hrsh7th/cmp-nvim-lsp" }
	use { "hrsh7th/cmp-nvim-lua" }

	-- Snippets
	use { "L3MON4D3/LuaSnip" }          --snippet engine
	use { "rafamadriz/friendly-snippets" } -- a bunch of snippets to use

	-- LSP
	use { "neovim/nvim-lspconfig" }        -- enable LSP
	use { "williamboman/mason.nvim" }      -- simple to use language server installer
	use { "williamboman/mason-lspconfig.nvim" }

  -- Visualize lsp progress
  use({
    "j-hui/fidget.nvim",
    config = function()
      require("fidget").setup()
    end
  })
	-- use { "jose-elias-alvarez/null-ls.nvim" } -- for formatters and linters
	use { "RRethy/vim-illuminate" }
	use { "VonHeikemen/lsp-zero.nvim" }
	use { "fatih/vim-go" }

	-- Games / improve my vimming
	use {'ThePrimeagen/vim-be-good'}
end)
